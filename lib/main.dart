import 'dart:math';

import 'package:flutter/material.dart';
import 'package:sa3_liquid/liquid/plasma/plasma.dart';

void main() {
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {

    return Material(
      color: Colors.black,
        child: Stack(
        children:  [
          //Background(),
          Center(
            child: RichText(
             text:   TextSpan(
                 style: TextStyle(color: Colors.white, fontSize: MediaQuery.of(context).size.height * 0.05),
                 children: [
               TextSpan(text: 'Problems or Questions about OoO Spots ? \n', style: TextStyle(fontWeight: FontWeight.bold, fontSize: MediaQuery.of(context).size.height * 0.07)),
               const TextSpan(text: 'Please contact us under : '),
               const TextSpan(text: 'Thales.Corp.O.Support@gmail.com', style: TextStyle(fontWeight: FontWeight.bold)),
             ]),
            ),
          ),
        ],
      ),
    );
  }
}


class Background extends StatelessWidget {
  const Background({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double sizeBackgroundPlasma = 0.35;

    Set<Color> colors = {
      Colors.green,
      Colors.blue,
      Colors.purple,
      Colors.orange,
      Colors.red,
      Colors.yellow,
      Colors.cyan,
    };

    List<Widget> stackList = [
    ];

    for (int index=0;index < colors.length; index++) {
      double offset= (6.28 / colors.length) *
          (index + 1);
      stackList.add(BackgroundPlasma(color: colors.elementAt(index), offset: offset, size: sizeBackgroundPlasma));
    }

    return Stack(
        children: stackList,
      );
  }
}

class BackgroundPlasma extends StatelessWidget {
  final Color color;
  final double offset;
  final double size;
  final double variationOne;
  final double variationTwo;
  final double variationThree;
  final double speed;
  final double blur;
  final int particles;
  final int fps;
  final BlendMode blendMode; //srcATop
  final ParticleType particleType;
  final double rotation;
  final PlasmaType plasmaType;

  const BackgroundPlasma(
      {required this.color,
      required this.offset,
      required this.size,
      this.variationOne = 0.7,
      this.variationTwo = 0.0,
      this.variationThree = 0.0,
      this.speed = 1.5,
      this.blendMode = BlendMode.srcATop,
      this.plasmaType = PlasmaType.circle,
      this.particleType = ParticleType.atlas,
      this.blur = 0.5,
      this.particles = 2,
      this.fps = 60,
      this.rotation = 0.0,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PlasmaRenderer(
      color: color,
      size: size,
      offset: offset,
      variation1: variationOne,
      variation2: variationTwo,
      variation3: variationThree,
      speed: speed,
      fps: fps,
      blur: blur,
      rotation: rotation,
      particles: particles,
      particleType: particleType,
      type: plasmaType,
      blendMode: blendMode,
    );
  }
}
